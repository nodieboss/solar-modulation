##These are small functions that are used in various portions of the program -- Generally they are called in the pre-run stage while we set things up
import math
import sys
import numpy as np
from scipy import interpolate

#amu_to_mass_GeV = 0.93149410242 ##1 dalton to GeV
#amu_to_mass_GeV = 1.00728

##Note, these conversion operators are overloaded -- you can pass in a numpy array of values, and will get back a numpy array of values
def convert_rigidity_to_ekin_per_nucleon(in_rigidity, in_mass, in_charge, in_nucleons):
    if(in_nucleons == 0): ##For electrons/positrons we don't want this term
        in_nucleons = 1.0
    sqrt_term = np.sqrt(np.power(in_rigidity*in_charge, 2.0) + np.power(in_mass, 2.0))
    return (sqrt_term-in_mass)/in_nucleons

def convert_rigidity_to_ekin(in_rigidity, in_mass, in_charge):
    sqrt_term = np.sqrt(np.power(in_rigidity*in_charge, 2.0) + np.power(in_mass, 2.0))
    return (sqrt_term-in_mass)

def convert_ekin_per_nucleon_to_rigidity(in_ekin, in_mass, in_charge, in_nucleons):
    if(in_nucleons == 0): ##For electrons/positrons we don't want this term
        in_nucleons = 1.0 ##For electrons, we use 1 in this calculation
    numerator_term = np.power(in_nucleons*in_ekin + in_mass, 2.0) - np.power(in_mass, 2.0)
    return np.sqrt(numerator_term/np.power(in_charge, 2.0))

def convert_ekin_to_rigidity(in_ekin, in_mass, in_charge):
    numerator_term = np.power(in_ekin + in_mass, 2.0) - np.power(in_mass, 2.0)
    return np.sqrt(numerator_term/np.power(in_charge, 2.0))

def convert_ekin_to_beta(in_ekin, in_mass):
    total_energy = in_ekin + in_mass ##both in GeV
    gamma = total_energy / in_mass ##definition of gamma
    beta = np.sqrt(1 - 1.0/np.power(gamma, 2.0))
    return beta

def convert_ekin_to_ekin_per_nucleon(in_ekin, in_nucleons):
    return in_ekin/in_nucleons ##Simple!

def convert_ekin_per_nucleon_to_ekin(in_ekin_per_nucleon, in_nucleons):
    if(in_nucleons == 0):
        in_nucleons = 1.0 ##for electrons or positrons
    return in_ekin_per_nucleon * float(in_nucleons)

def convert_flux_total_to_differential(flux, average_energy, minimum_energy, maximum_energy):
    return flux / (maximum_energy-minimum_energy) ##Don't even need to know what the average energy actually is

def convert_flux_integral_to_differential(flux, average_energy, minimum_energy, maximum_energy):
    return flux / average_energy

def convert_flux_differential_to_integral(flux, average_energy, minimum_energy, maximum_energy):
    return flux * average_energy

def convert_flux_differential_to_total(flux, average_energy, minimum_energy, maximum_energy):
    return flux * (maximum_energy-minimum_energy)

def convert_differential_flux_rigidity_to_ekin_pn(flux, average_rigidity, in_mass, in_charge, in_nucleons):
    ##Do this by taking a very small rigidity difference, and then calculating the energy difference, and then dividing them
    norm_difference=1.0e-8 ##Very small compared to most cases we will run into, but much bigger than the numerical accuracy of the problem
    e1 = convert_rigidity_to_ekin_per_nucleon(average_rigidity-norm_difference, in_mass, in_charge, in_nucleons)
    e2 = convert_rigidity_to_ekin_per_nucleon(average_rigidity+norm_difference, in_mass, in_charge, in_nucleons)
    return flux * 2.0 * norm_difference / (e2-e1) ##If e2-e1 is bigger the flux/dE goes down

def convert_differential_flux_rigidity_to_ekin(flux, average_rigidity, in_mass, in_charge):
    ##Do this by taking a very small rigidity difference, and then calculating the energy difference, and then dividing them
    norm_difference=1.0e-8 ##Very small compared to most cases we will run into, but much bigger than the numerical accuracy of the problem
    e1 = convert_rigidity_to_ekin(average_rigidity-norm_difference, in_mass, in_charge)
    e2 = convert_rigidity_to_ekin(average_rigidity+norm_difference, in_mass, in_charge)
    return flux * 2.0 * norm_difference / (e2-e1) ##If e2-e1 is bigger the flux/dE goes down

def convert_differential_flux_rigidity_to_ekin_pn_full_bin(flux, minimum_rigidity, maximum_rigidity, mass, charge, in_nucleons):
    e1 = convert_rigidity_to_ekin_per_nucleon(minimum_rigidity, in_mass, in_charge, in_nucleons)
    e2 = convert_rigidity_to_ekin_per_nucleon(maximum_rigidity, in_mass, in_charge, in_nucleons)
    return flux * (maximum_rigidity - minimum_rigidity) / (e2-e1)

def convert_differential_flux_rigidity_to_ekin_full_bin(flux, minimum_rigidity, maximum_rigidity, mass, charge):
    e1 = convert_rigidity_to_ekin_per_nucleon(minimum_rigidity, in_mass, in_charge)
    e2 = convert_rigidity_to_ekin_per_nucleon(maximum_rigidity, in_mass, in_charge)
    return flux * (maximum_rigidity - minimum_rigidity) / (e2-e1)

def convert_differential_flux_ekin_to_rigidity(flux, average_ekin, in_mass, in_charge):
    norm_difference=1.0e-8
    e1 = convert_ekin_to_rigidity(average_ekin-norm_difference, in_mass, in_charge)
    e2 = convert_ekin_to_rigidity(average_ekin+norm_difference, in_mass, in_charge)
    return flux * 2.0 * norm_difference / (e2-e1)

def convert_differential_flux_ekin_to_ekin_pn(flux, average_ekin, particle_nucleons):
    if(particle_nucleons == 0): ##These are electrons/positrons, and here we just want to ignore the issue
        particle_nucleons = 1.0
    return flux / particle_nucleons

def convert_differential_flux_ekin_pn_to_ekin(flux, average_ekin_pn, particle_nucleons):
    if(particle_nucleons == 0): ##These are electrons/positrons, and here we just want to ignore the issue
        particle_nucleons = 1.0
    return flux * particle_nucleons


##This checks whatever energy unit we have, and then calculates the rigidity, ekin, ekin_pn and convert_ekin_to_beta
##Doing this analysis here takes it out of the modulate_dataset_at_datapoint, which might be called multiple times with different values of phi0 and phi1, saving time.
def convert_ekin_to_all(ekin_avg, analysis_options):
    particle_mass = float(analysis_options['particle_mass'])
    particle_charge = float(analysis_options['particle_charge'])
    particle_nucleons = float(analysis_options['particle_nucleons'])

    ekin = ekin_avg
    rigidity=convert_ekin_to_rigidity(ekin_avg, particle_mass, particle_charge)
    if(particle_nucleons == 0): ##For electrons, this is the same
        ekin_per_nucleon = ekin_avg
    else:
        ekin_per_nucleon = ekin_avg / particle_nucleons
    beta = convert_ekin_to_beta(ekin, particle_mass)

    return(rigidity, ekin, ekin_per_nucleon, beta)


##This figures out the months that we want to average over, and then figures out the average weights, magnetic field strengths, and alpha values we want to use
def calculate_effective_modulation_potentials(analysis_options, solar_data):
    ##Column list, to be explicit
    Column_Bartels_Rotation = 0
    Column_Starttime = 1
    Column_Endtime=2
    Column_Bfield=3
    Column_Tilt=5
    Column_Rigidity_Positive_Normalization=6
    Column_Rigidity_Negative_Normalization=7
    solar_model_parameters = []
    analysis_starttime = analysis_options['observation_starttime']
    analysis_endtime = analysis_options['observation_endtime']
    totalweight=0

    ##Figure out whether we are going to use the negative or positive rigidity terms -- so we only need to check this once and don't need to send it to the test function
    for timestep in range(0, len(solar_data)):
        ##There are four options that produce non-zero contributions
        ##### starttime, solar_data_start, solar_data_end, endtime = 1.0
        ##### solar_data_start, starttime, solar_data_end, endtime = solar_data_end - starttime / (solar_data_end-solar_data_start) ##Need to check extra conditions
        ##### starttime, solar_data_start, endtime, solar_data_end = endtime-solar_data_start / solar_data_end-solar_data_start ##Need to check third condition
        ##### solar_data_start, starttime, endtime, solar_data_end = endtime-starttime / solar_data_end - solar_data_start (when the whole thing is less than 1 Bartels rotation)
        inweight = 0
        if(solar_data[timestep][Column_Starttime] > analysis_starttime and solar_data[timestep][Column_Endtime] < analysis_endtime):
            inweight = solar_data[timestep][Column_Endtime] - solar_data[timestep][Column_Starttime] ##Number of days
            totalweight += inweight
        if(solar_data[timestep][Column_Starttime] < analysis_starttime and solar_data[timestep][Column_Endtime] > analysis_starttime and solar_data[timestep][Column_Endtime] < analysis_endtime):
            inweight = solar_data[timestep][Column_Endtime] - analysis_starttime
            totalweight += inweight
        if(solar_data[timestep][Column_Starttime] > analysis_starttime and solar_data[timestep][Column_Endtime] > analysis_endtime and solar_data[timestep][Column_Starttime] < analysis_endtime):
            inweight = analysis_endtime - solar_data[timestep][Column_Starttime]
            totalweight += inweight
        if(solar_data[timestep][Column_Starttime] < analysis_starttime and solar_data[timestep][Column_Endtime] > analysis_endtime):
            inweight += analysis_endtime - analysis_starttime
            totalweight += inweight

        if(inweight > 0): ##Speed this up by ignoring lines we don't care about
            bartels_rotation_number_val = solar_data[timestep][8]
            ##Now need to get the B-field by offsetting the Bartels Rotations
            ##Need to sum both positive and negative contributions to the B-field value
            average_bfield_value = 0.0
            average_tilt_value = 0.0
            if(float(analysis_options['particle_charge']) > 0):
                Column_Rigidity = Column_Rigidity_Positive_Normalization
            else:
                Column_Rigidity = Column_Rigidity_Negative_Normalization
            if(solar_data[timestep][Column_Rigidity] > 0.0): ##We have a positive normilization term, this includes the phi1 term, and is generally more delayed
                number_Bfield_Positive_Offsets = 1.0*len(analysis_options['B_delay_positive'])
                number_tilt_Positive_offsets = 1.0 * len(analysis_options['Tilt_delay_positive'])
                for Bfield_Offset in analysis_options['B_delay_positive']:
                    average_bfield_value += 1.0 * solar_data[timestep-Bfield_Offset][Column_Bfield] / number_Bfield_Positive_Offsets * solar_data[timestep][Column_Rigidity]
                    #print("Positive B: ", bartels_rotation_number_val, Bfield_Offset, solar_data[timestep][Column_Rigidity])
                for Tilt_Offset in analysis_options['Tilt_delay_positive']:
                    average_tilt_value += 1.0 * solar_data[timestep-Tilt_Offset][Column_Tilt] / number_tilt_Positive_offsets * solar_data[timestep][Column_Rigidity]
                    #print("Positive Tilt: ", bartels_rotation_number_val , Tilt_Offset, 1.0 - solar_data[timestep][Column_Rigidity])
            if(1.0 - solar_data[timestep][Column_Rigidity] > 0.0): ##We have a negative normilization term, we can have both
                number_Bfield_Negative_Offsets = 1.0*len(analysis_options['B_delay_negative'])
                number_tilt_Negative_offsets = 1.0 * len(analysis_options['Tilt_delay_negative'])
                for Bfield_Offset in analysis_options['B_delay_negative']:
                    #print("Negative B: ", bartels_rotation_number_val , Bfield_Offset, 1.0 - solar_data[timestep][Column_Rigidity])
                    average_bfield_value += 1.0 * solar_data[timestep-Bfield_Offset][Column_Bfield] / number_Bfield_Negative_Offsets * (1.0 - solar_data[timestep][Column_Rigidity])
                for Tilt_Offset in analysis_options['Tilt_delay_negative']:
                    average_tilt_value += 1.0 * solar_data[timestep-Tilt_Offset][Column_Tilt] / number_tilt_Negative_offsets * (1.0 - solar_data[timestep][Column_Rigidity])
                    #print("Negative Tilt: ", bartels_rotation_number_val , Tilt_Offset, 1.0 - solar_data[timestep][Column_Rigidity])

            ##Now determine whether we want the negative or positive rigidity_dependent term
            rigidity_dependent_term = solar_data[timestep][Column_Rigidity]
            #print("Rigidity Dependent Term: ", rigidity_dependent_term )

            solar_model_parameters += [[average_bfield_value, average_tilt_value, rigidity_dependent_term, inweight, bartels_rotation_number_val]] ##Bfield, Tilt, Weight, Bartels Rotation Number (for bookkeeping)
    solar_model_parameters = np.asarray(solar_model_parameters)
    return (solar_model_parameters, totalweight)


##In this function, we need to undo all the unit conversion we did previously, in order to get things back into the units that people started with
##The fluxes are currently now differential fluxes in Ekin, and we have all of the values defined at their average positions here
##The minimum, average, and maximum energies are the units that were given at the beginning
##Note: The only thing we need to modify at this step is the fluxes,
def convert_back_to_input_units(fluxes, yaml_options, rigidity, ekin, ekin_per_nucleon, beta, minimum_energy, average_energy, maximum_energy):
    particle_mass = float(yaml_options['particle_mass'])
    particle_charge = float(yaml_options['particle_charge'])
    particle_nucleons = float(yaml_options['particle_nucleons'])

    if(yaml_options['ekin_or_rigidity'] == 'rigidity'):
        fluxes = convert_differential_flux_ekin_to_rigidity(fluxes, ekin, mass, charge)
    if(yaml_options['ekin_or_rigidity'] == 'ekin_pn'):
        fluxes = convert_differential_flux_ekin_to_ekin_pn(fluxes, ekin, particle_nucleons)

    ##Now we want to go back to total or integral fluxes if that is what we are doing
    ##We now have fluxes in the right rigidity/ekin_pn/ekin units so we can use the average/minimum/maximum definitions, since they ar ethe same
    if(yaml_options['differential_integral_or_total'] == 'total'):
        fluxes = modulate_functions.convert_flux_differential_to_total(fluxes, average_energy, minimum_energy, maximum_energy)

    if(yaml_options['differential_integral_or_total'] == 'integral'):
        fluxes = modulate_functions.convert_flux_differential_to_integral(fluxes, average_energy, minimum_energy, maximum_energy)

    if(yaml_options['differential_integral_or_total'] == 'differential'):
        fluxes = fluxes

    ##Finally, we need to convert to the right KeV/MeV/GeV/TeV/eV units
    ##Here we did update the energy definitions, so we need to set them back

    if(yaml_options['energy_units'] == 'e'):
        minimum_energy = minimum_energy*1.0e9
        maximum_energy = maximum_energy*1.0e9
        average_energy = average_energy*1.0e9
        if(yaml_options['differential_integral_or_total'] == 'differential'): ##need to update this if it is differential
            fluxes = fluxes / 1.0e9 ##opposite conversion

    if(yaml_options['energy_units'] == 'k'):
        minimum_energy = minimum_energy*1.0e6
        maximum_energy = maximum_energy*1.0e6
        average_energy = average_energy*1.0e6
        if(yaml_options['differential_integral_or_total'] == 'differential'): ##need to update this if it is differential
            fluxes = fluxes / 1.0e6 ##opposite conversion

    if(yaml_options['energy_units'] == 'M'):
        minimum_energy = minimum_energy*1.0e3
        maximum_energy = maximum_energy*1.0e3
        average_energy = average_energy*1.0e3
        if(yaml_options['differential_integral_or_total'] == 'differential'): ##need to update this if it is differential
            fluxes = fluxes / 1.0e3 ##opposite conversion

    if(yaml_options['energy_units'] == 'T'):
        minimum_energy = minimum_energy/1.0e3
        maximum_energy = maximum_energy/1.0e3
        average_energy = average_energy/1.0e3
        if(yaml_options['differential_integral_or_total'] == 'differential'): ##need to update this if it is differential
            fluxes = fluxes * 1.0e3 ##opposite conversion

    return (fluxes, minimum_energy, average_energy, maximum_energy)
