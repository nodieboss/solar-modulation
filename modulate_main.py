##This code takes an input cosmic-ray spectrum spectrum, with parameters determined by analysis_options.yaml
##And Returns an output cosmic-ray spectrum at Earth
##See text in analysis_options.yaml for more information about how to use the code

import numpy as np
import sys
import math
from scipy import interpolate

import modulate_fileio
import modulate_functions
import modulate_fit

##Some generic constants, but made explicit here
yaml_filename = 'analysis_options.yaml'
particle_filename = 'particle_inputs.yaml'
solar_data_filename = 'solardata_inputs.txt'



################################################
######### Main Program #########################

##The first four steps are pre-run
#Step 1: Read YAML Options
yaml_options = modulate_fileio.read_yaml_file(yaml_filename)

#Step 2: Read The Particle File -- if the yaml option species a particle, reset the particle information to match the database - so this returns yaml_options again
yaml_options = modulate_fileio.read_particle_file(yaml_options, particle_filename)

##Print a statement out so people can check that the code is picking up things properly
print ("We are assuming the file is in:", yaml_options['ekin_or_rigidity'])

#Step 3: Read the input ISM file -- convert the units so that everything is in GeV, the flux is in differential units, and the energy scale is user specified (rigidity, ekin, ekin_pn, etc.)
(minimum_energy, average_energy, maximum_energy, flux, ekin_average, differential_flux_ekin, ism_flux_interpolation_object, num_columns_for_output) = modulate_fileio.read_input_file(yaml_options)

#Step 3b: Regardless of what the energy scale is -- get all of the right values at the average energy, because we will need them in various parts of the convert_rigidity_to_ekin
(rigidity, ekin, ekin_per_nucleon, beta) = modulate_functions.convert_ekin_to_all(ekin_average, yaml_options)

#Step 4: Get the information regarding the solar data
solar_data = modulate_fileio.read_solar_data(solar_data_filename)

#Step 4b: Using information on the delay distribution of the potential, get a list of all the Bartels rotations (and their relative weights) in our analysis, and calculate the effective B-tot and tilt angle in each
(solar_model_parameters, solar_model_totalweight) = modulate_functions.calculate_effective_modulation_potentials(yaml_options, solar_data)

##The next steps could happen once (if we are evaluating only one phi0/phi1, or could happen many times if we are scanning a parameter space)
##Note: This part of the code occurs in Ekin
modulation_potential_array = modulate_fit.calculate_modulation_potentials_for_phi0_phi1__and__transfer_modulation_potential_ism_to_earth(float(yaml_options['phi0']), float(yaml_options['phi1']), rigidity, ekin, beta, float(yaml_options['particle_mass']), np.fabs(float(yaml_options['particle_charge'])), solar_model_parameters )

fluxes = modulate_fit.calculate_ekin_at_earth_from_ekin_ism_using_modulation_parameters(ekin, ism_flux_interpolation_object, modulation_potential_array, solar_model_totalweight, float(yaml_options['particle_mass']), np.fabs(float(yaml_options['particle_charge'])))

(fluxes_base_units, minimum_energy_base_units, average_energy_base_units, maximum_energy_base_units) = modulate_functions.convert_back_to_input_units(fluxes, yaml_options, rigidity, ekin, ekin_per_nucleon, beta, minimum_energy, average_energy, maximum_energy)

average_flux_over_full_observation_base_units = modulate_fit.get_average_spectrum_over_full_observation(fluxes_base_units, solar_model_parameters)


##Now print the output files we want,
modulation_potential_interpolated= modulate_fit.calculate_modulation_potentials_for_phi0_phi1(float(yaml_options['phi0']), float(yaml_options['phi1']), rigidity, ekin, beta, solar_model_parameters, yaml_options['interpolation_order'])
modulate_fileio.write_output_spectra(yaml_options, minimum_energy_base_units, average_energy_base_units, maximum_energy_base_units, average_flux_over_full_observation_base_units, fluxes_base_units,  solar_model_parameters, num_columns_for_output)
if(yaml_options['print_modulation_potential_file'] == True):
    modulate_fileio.write_modulation_potential_files(yaml_options, ekin, modulation_potential_interpolated, solar_model_parameters)
