##These functions control file input/output

import yaml
import numpy as np
from scipy import interpolate

import modulate_functions

def read_yaml_file(yaml_filename):
    yaml_options = {} ##blank dictionary that we will add entries to

	##Now open the YAML file and read in the input configuration files
    with open(yaml_filename, 'r') as stream:
        input_parameters = yaml.safe_load(stream)

	##Now load the input parameters into the correct variables
	##analysis routines
    yaml_options['mpi_cores'] = int(input_parameters['program_options']['mpi_cores'])

    yaml_options['input_filename'] = input_parameters['input_file']['input_filename']

    yaml_options['ekin_or_rigidity'] = input_parameters['input_file']['ekin_or_rigidity']
    if(yaml_options['ekin_or_rigidity'] != 'ekin' and yaml_options['ekin_or_rigidity'] != 'rigidity' and yaml_options['ekin_or_rigidity'] != 'ekin_pn'):
        raise ValueError("Unknown Energy Scale Specified as 'ekin_or_rigidity' in yaml file. Quitting...")

    yaml_options['energy_units'] = input_parameters['input_file']['energy_units']
    if(yaml_options['energy_units'] != 'G' and yaml_options['energy_units'] != 'M' and yaml_options['energy_units'] != 'T' and yaml_options['energy_units'] != 'k' and yaml_options['energy_units'] != 'e'):
        raise ValueError("Unknown Energy Units Specified as 'energy_units' in yaml file. Quitting...")

    yaml_options['particle_name'] = input_parameters['input_file']['particle_name']
    yaml_options['particle_nucleons'] = float(input_parameters['input_file']['particle_nucleons'])
    yaml_options['particle_mass'] = float(input_parameters['input_file']['particle_mass'])
    yaml_options['particle_charge'] = float(input_parameters['input_file']['particle_charge'])
    yaml_options['differential_integral_or_total'] = input_parameters['input_file']['differential_integral_or_total']
    if(yaml_options['differential_integral_or_total'] != 'differential' and yaml_options['differential_integral_or_total'] != 'integral' and yaml_options['differential_integral_or_total'] != 'total'):
        raise ValueError("Unknown Option in 'Differential_integral_or_total'. Quiting...")
    yaml_options['energy_spacing_log_or_linear'] = input_parameters['input_file']['energy_spacing_log_or_linear']
    if(yaml_options['energy_spacing_log_or_linear'] != 'log' and yaml_options['energy_spacing_log_or_linear'] != 'linear'):
        raise ValueError("Unknown Option in 'energy_spacing_log_or_linear'. Quiting...")
    yaml_options['average_bin_position'] = input_parameters['input_file']['average_bin_position']
    if(yaml_options['average_bin_position'] != 'log'):
        yaml_options['numeric_average_bin_position'] = True
        yaml_options['average_bin_position'] = float(input_parameters['input_file']['average_bin_position'])
    else:
        yaml_options['numeric_average_bin_position'] = False
    yaml_options['derivative_local_or_bin'] = input_parameters['input_file']['derivative_local_or_bin']
    if(yaml_options['derivative_local_or_bin'] != 'local' and yaml_options['derivative_local_or_bin'] != 'bin'):
        raise ValueError("Unknown Option in derivative_local_or_bin. Quitting")
    yaml_options['interpolation_order'] = input_parameters['input_file']['interpolation_order']
    if(yaml_options['interpolation_order'] != 'linear' and yaml_options['interpolation_order'] != 'cubic'):
        raise ValueError("Unknown option in 'interpolation_order'. Quitting.")
    yaml_options['bestfit_or_errorbands'] = input_parameters['solar_modulation_parameters']['bestfit_or_errorbands']
    if(yaml_options['bestfit_or_errorbands'] != 'bestfit' and yaml_options['bestfit_or_errorbands'] != 'errorbands'):
        raise ValueError("Unknown Option in 'bestfit_or_errorbands'. Quitting...")
    yaml_options['output_filename'] = input_parameters['output_file']['output_filename']
    yaml_options['print_time_series_results'] = bool(input_parameters['output_file']['print_time_series_results'])
    yaml_options['print_modulation_potential_file'] = bool(input_parameters['output_file']['print_modulation_potential_file'])
    yaml_options['modulation_potential_file_bins_per_decade'] = int(input_parameters['output_file']['modulation_potential_file_bins_per_decade'])
    yaml_options['errorbands_accuracy'] = int(input_parameters['solar_modulation_parameters']['errorbands_accuracy'])
    yaml_options['phi0'] = float(input_parameters['solar_modulation_parameters']['phi0'])
    yaml_options['phi1'] = float(input_parameters['solar_modulation_parameters']['phi1'])
    yaml_options['observation_starttime'] = float(input_parameters['solar_modulation_parameters']['observation_starttime'])
    yaml_options['observation_endtime'] = float(input_parameters['solar_modulation_parameters']['observation_endtime'])
    yaml_options['B_delay_positive'] = input_parameters['solar_modulation_parameters']['B_delay_positive'] ##Should be a list of numbers
    yaml_options['B_delay_negative'] = input_parameters['solar_modulation_parameters']['B_delay_negative'] ##Should be a list of numbers
    yaml_options['Tilt_delay_positive'] = input_parameters['solar_modulation_parameters']['Tilt_delay_positive'] ##Should be a list of numbers
    yaml_options['Tilt_delay_negative'] = input_parameters['solar_modulation_parameters']['Tilt_delay_negative'] ##Should be a list of numbers

    return yaml_options

##This reads the particle_inputs.yaml file, which specifies a bunch of nuclei files, along with their masses, nucleons, and charges
##If the filename matches the input filename from analysis_options.yaml, it updates the yaml_options to reflect the datafile Here
##If it does not, then it does nothing
##It returns a (possibly) updated version of yaml options, with consistent particle information
##It reports to the user whether the particle name, or the input mass/nucleons/charge are being used.
def read_particle_file(yaml_options, particle_filename):
    particle_options = {} ##blank dictionary that we will add entries to

	##Now open the YAML file and read in the input configuration files
    with open(particle_filename, 'r') as stream:
        input_parameters = yaml.safe_load(stream)

    try:
        ##Assume that yaml_options['particle_name'] is a valud name, and reset things, this will fail immediately
        yaml_options['particle_nucleons'] = input_parameters[yaml_options['particle_name']]['particle_nucleons']
        yaml_options['particle_mass'] = input_parameters[yaml_options['particle_name']]['particle_mass']
        yaml_options['particle_charge'] = input_parameters[yaml_options['particle_name']]['particle_charge']
        print ("Found the particle name", "'" + yaml_options['particle_name'] + "'", "Utilizing the Default Options from particle_inputs.")
    except:
        if(yaml_options['particle_name'] == 'null'):
            print("Did not find particle name. Using User specified options")
        else:
            raise ValueError("The value 'particle_name' in analysis_options.yaml is not a valid name, and it is also not 'null'. Quitting...")
    print("Options Utilized are:")
    print("Particle Nucleons:", yaml_options['particle_nucleons'])
    print("Particle Mass:", yaml_options['particle_mass'], "GeV")
    print("Particle Charge:", yaml_options['particle_charge'])
    return yaml_options



## This opens the input file, takes into account the options specified in the yaml file
## It keeps track of what the original units are in the analysis. It also converts things to Ekin and dN/delta_Ekin
def read_input_file(yaml_options):
    ##First open the file and get all of the information out of it
    try:
        ism_spectral_file = open(yaml_options['input_filename'], 'r')
    except:
        raise ValueError("Can't find ISM Spectral File", yaml_options['input_filename'], "Quitting...")

    num_energy_bins=0 ##This is the number of energy bins we find
    num_columns=0 ##This is the number of columns we found, 2 means that the energy is an average, 3 means it is min/max - if it changes, trigger an error
    for line in ism_spectral_file.readlines():
        if(len(line) > 2 and line[0] != '#'): ##Ignore lines that are commented out, or lines that have nothing in them
            params = line.split()

            ##Test that everything looks good if this is the first line we are reading. If it is not, test that nothing has changed
            if(num_energy_bins == 0):
                num_columns = len(params)
                if(num_columns == 2):
                    average_energy = []
                    flux = []
                elif(num_columns == 3):
                    minimum_energy = []
                    maximum_energy = []
                    flux = []
                else:
                    raise ValueError("The Number of Columns in the ISM Spectral File is not 2 or 3. I don't know how to deal with this. Quitting...")
            else:
                test_num_columns = len(params)
                if(test_num_columns != num_columns):
                    raise ValueError("The number of Columns in the ISM Spectral File Changed? I don't know how to deal with this. Quitting...")

            if(num_columns == 2):
                average_energy += [float(params[0])]
                flux += [float(params[1])]
            if(num_columns == 3):
                minimum_energy += [float(params[0])]
                maximum_energy += [float(params[1])]
                flux += [float(params[2])]
            num_energy_bins += 1
    ism_spectral_file.close()

    ##Now go through and get the standardize the units to things we want to work with
    ##That is - we want a rigidity_low, rigidity_avg, rigidity_high, and a number of particles in this bin
    ##We want Rigidities to be defined in GV, and the number of particles in the bin to be a unitless number

    ##First convert everything to numpy arrays, so we can deal with them easier
    if(num_columns == 2):
        average_energy = np.asarray(average_energy)
    if(num_columns == 3):
        minimum_energy = np.asarray(minimum_energy)
        maximum_energy = np.asarray(maximum_energy)
    flux = np.asarray(flux)

    ##Fix up the average energy now -- because we assume it is the logarithmic or linear average, we don't need units to fix it
    if(num_columns == 2):
        minimum_energy = []
        maximum_energy = []
        if(yaml_options['energy_spacing_log_or_linear'] == 'linear'):
            linear_energy_difference = average_energy[1] - average_energy[0] ##You need two rows at least, which makes sense
            for energy_bin in range(0, len(average_energy)):
                minimum_energy += [average_energy[energy_bin] - linear_energy_difference/2.0]
                maximum_energy += [average_energy[energy_bin] + linear_energy_difference/2.0]
        if(yaml_options['energy_spacing_log_or_linear'] == 'log'):
            logarithmic_energy_difference = average_energy[1] / average_energy[0]
            for energy_bin in range(0, len(average_energy)):
                minimum_energy += [average_energy[energy_bin] / np.sqrt(logarithmic_energy_difference)]
                maximum_energy += [average_energy[energy_bin] * np.sqrt(logarithmic_energy_difference)]
        if(yaml_options['energy_spacing_log_or_linear'] == 'power'):
            for energy_bin in range(0, len(average_energy)):
                minim
        minimum_energy = np.asarray(minimum_energy)
        maximum_energy = np.asarray(maximum_energy)

    if(num_columns == 3): ##Now go backwards and calculate the average energy from the minimum and maximum energy data,
        average_energy = []
        if(yaml_options['energy_spacing_log_or_linear'] == 'linear'):
            for energy_bin in range(0, len(minimum_energy)):
                average_energy += [minimum_energy[energy_bin] - maximum_energy[energy_bin]]
        if(yaml_options['energy_spacing_log_or_linear'] == 'log'):
            logarithmic_energy_difference = maximum_energy[0] / minimum_energy[0] ##assume constant logarithmic energy spacing
            for energy_bin in range(0, len(minimum_energy)):
                average_energy += [np.sqrt(minimum_energy[energy_bin] * maximum_energy[energy_bin])]
#        if(yaml_options['energy_spacing_log_or_linear'] == 'power'):
#            spectrum_plus_one = yaml_options['average_bin_position'] + 1.0 ##Integrate, so add one to spectrum
#            average_energy += [np.power(0.5 * np.power(minimum_energy[energy_counter], spectrum_plus_one) + 0.5*np.power(maximum_energy[energy_counter], spectrum_plus_one), 1.0/spectrum_plus_one)]


    ##Then convert everything to GV/GeV units -- check the analysis options, and if it is 'k', 'M', 'T', or 'e', make the necessary conversion
    ##Whether or not the flux is converted depends on whether the unit is differnetial or not, so check this.
    ##These conversions are good regardless of whether we deal with rigidity or Ekin, or Ekin_pn
    if(yaml_options['energy_units'] == 'T'):
        minimum_energy = minimum_energy*1000.0
        maximum_energy = maximum_energy*1000.0
        average_energy = average_energy*1000.0
        if(yaml_options['differential_integral_or_total'] == 'differential'): ##need to update this if it is differential
            flux = flux / 1000.0 ##opposite conversion

    if(yaml_options['energy_units'] == 'M'):
        minimum_energy = minimum_energy/1000.0
        maximum_energy = maximum_energy/1000.0
        average_energy = average_energy/1000.0
        if(yaml_options['differential_integral_or_total'] == 'differential'): ##need to update this if it is differential
            flux = flux * 1000.0 ##opposite conversion

    if(yaml_options['energy_units'] == 'k'):
        minimum_energy = minimum_energy/1.0e6
        maximum_energy = maximum_energy/1.0e6
        average_energy = average_energy/1.0e6
        if(yaml_options['differential_integral_or_total'] == 'differential'): ##need to update this if it is differential
            flux = flux * 1.0e6 ##opposite conversion

    if(yaml_options['energy_units'] == 'e'):
        minimum_energy = minimum_energy/1.0e9
        maximum_energy = maximum_energy/1.0e9
        average_energy = average_energy/1.0e9
        if(yaml_options['differential_integral_or_total'] == 'differential'): ##need to update this if it is differential
            flux = flux * 1.0e9 ##opposite conversion

    ##At this point, we have a bunch of energies and fluxes. We have done some transformations that are applicable to all inputs
    ##However, there is some uncertainty in what the units are, there are a few options:
    ## GV, GV, GV^-1
    ## GV, GV, GV * GV^-1
    ## GV, GV, number
    ## GeV, GeV, GeV^-1
    ## GeV, GeV, GeV * GeV^-1
    ## GeV, GeV, number

    ##The final thing to do is convert everything to a differntial flux that is in Ekin, because this is how the solar modulation potential is defined
    ####We will need to track the conversions we did, because we will need to send them back later
    ##Then, we will also send back a linear interpolation array, allowing us to call the flux at various values to increase accuracy

    ##The first step is to convert everything to a differential flux

    if(yaml_options['differential_integral_or_total'] == 'total'):
        differential_flux = modulate_functions.convert_flux_total_to_differential(flux, average_energy, minimum_energy, maximum_energy)

    if(yaml_options['differential_integral_or_total'] == 'integral'):
        differential_flux = modulate_functions.convert_flux_integral_to_differential(flux, average_energy, minimum_energy, maximum_energy)

    if(yaml_options['differential_integral_or_total'] == 'differential'):
        differential_flux = flux

    ##The second step is to convert the derivative ekin / ekin_per_nucleon (easy) and derivative of ekin/rigidity (somewhat harder) at each value of the energy_avg
    if(yaml_options['ekin_or_rigidity'] == 'ekin_pn'):
        number_of_nucleons = float(yaml_options['particle_nucleons'])
        ekin_average = modulate_functions.convert_ekin_per_nucleon_to_ekin(average_energy, number_of_nucleons)
        differential_flux_ekin = modulate_functions.convert_differential_flux_ekin_pn_to_ekin(differential_flux, average_energy, number_of_nucleons)

    ##We are now faced with sort of a choce -- if the input file had central values, it is most correct to just find the derivative at the central value and calculate it
    ##The option 'derivative_local_or_bin' asks the user which they want to use
    if(yaml_options['ekin_or_rigidity'] == 'rigidity'):
        ##We also need to get the average energy, to make our interpolation object
        mass_of_particle = float(yaml_options['particle_mass'])
        charge_of_particle = float(yaml_options['particle_charge'])
        nucleons_of_particle = float(yaml_options['particle_nucleons'])
        ekin_average = modulate_functions.convert_rigidity_to_ekin(average_energy, mass_of_particle, charge_of_particle)
        if(yaml_options['derivative_local_or_bin'] == 'local'):
            differential_flux_ekin = modulate_functions.convert_differential_flux_rigidity_to_ekin(differential_flux, average_energy, mass_of_particle, charge_of_particle)
        if(yaml_options['derivative_local_or_bin'] == 'bin'):
            differential_flux_ekin = modulate_functions.convert_differential_flux_rigidity_to_ekin_full_bin(differential_flux, minimum_energy, maximum_energy, mass_of_particle, charge_of_particle)

    ##Ekin is the unit we want
    if(yaml_options['ekin_or_rigidity'] == 'ekin'):
        ekin_average = average_energy
        differential_flux_ekin = differential_flux

    ##One complexity, is that the highest call to the ISM interpolation object will call something that is outside the scope of the problem. This cannot be fixed, except to add one more object that is extrapolated up, a warning about this behavior is in the file
    ism_flux_interpolation_object = interpolate.interp1d(ekin_average, differential_flux_ekin, kind=yaml_options['interpolation_order'], bounds_error=False, fill_value=differential_flux_ekin[-1]) ##This is a linear interpolation object, to make sure nothing fancy happens -- it specifies the flux in the units we want at any energy

    ##At this point, we have everything in its original units, binned and differential, and we also have an interpolation object in Ekin   and dN/dEkin
    return (minimum_energy, average_energy, maximum_energy, flux, ekin_average, differential_flux_ekin, ism_flux_interpolation_object, num_columns)

def read_solar_data(solar_data_filename):
    ##The columns we are reading in are Bartels Rotation, Start Time, End Time, Magnetic Field, Alpha
    solar_data_infile = open(solar_data_filename, 'r')
    solar_data = []
    for line in solar_data_infile.readlines():
        if(line[0] != '#' and len(line) > 2): ##Ignore commented out and blank lines
            params = line.split()
            bartels_rotation_number = float(params[0])
            ##Bartels Rotations are Explicitly 27 days, so we convert this to MJD start and End times as follows:
            starttime = (bartels_rotation_number - 2327.0) * 27.0 +  53023.00000000
            endtime = (bartels_rotation_number - 2327.0) * 27.0 +  53050.00000000
            solar_data += [[float(params[0]), starttime, endtime, float(params[1]), float(params[2]), float(params[3]), float(params[4]), float(params[5]), bartels_rotation_number]] ##This includes B-field uncertainties, in case they are useful later
    solar_data = np.asarray(solar_data) ##make this a numpy array
    return solar_data


def write_output_spectra(yaml_options, minimum_energy, average_energy, maximum_energy, average_flux_over_full_observation, fluxes, solar_model_parameters, num_columns_for_output):
    root_output_filename = yaml_options['output_filename']
    write_timeseries = yaml_options['print_time_series_results']

    ##Write with a central energy
    if(num_columns_for_output == 2):
        outfile = open(root_output_filename + '.txt', 'w')
        for a in range(0, len(average_energy)):
            print(average_energy[a], average_flux_over_full_observation[a], file=outfile)
        outfile.close()

        if(write_timeseries == True):
            for a in range(0, len(solar_model_parameters)): ##Go through every timestep
                outfile_name_timestep = root_output_filename + '_bartels_' + str(int(solar_model_parameters[a][4])) + '.txt'
                outfile = open(outfile_name_timestep, 'w')
                for b in range(0, len(average_energy)):
                    print(average_energy[b], fluxes[a][b], file=outfile)
                outfile.close()

    ##Write with minimum and maximum energies
    if(num_columns_for_output == 3):
        outfile = open(root_output_filename + '.txt', 'w')
        for a in range(0, len(minimum_energy)):
            print(minimum_energy[a], maximum_energy[a], average_flux_over_full_observation[a], file=outfile)
        outfile.close()

        if(write_timeseries == True):
            for a in range(0, len(solar_model_parameters)): ##Go through every timestep
                outfile_name_timestep = root_output_filename + '_bartels_' + str(int(solar_model_parameters[a][4])) + '.txt'
                outfile = open(outfile_name_timestep, 'w')
                for b in range(0, len(minimum_energy)):
                    print(minimum_energy[b], maximum_energy[b], fluxes[a][b], file=outfile)
                outfile.close()
    return 0

def write_modulation_potential_files(yaml_options, energy, modulation_potential_interpolated, solar_model_parameters):
        lgminimum_energy = np.log10(energy[0]+1.0e-12) ##once in awhile we got rounding errors - so add a very small offset to make sure this is more than the minimum bin of the interpolator
        lgmaximum_energy = np.log10(energy[-1])
        bins_per_decade = yaml_options['modulation_potential_file_bins_per_decade']
        root_output_filename = yaml_options['output_filename']
        lg_energy_bins = np.arange(lgminimum_energy, lgmaximum_energy, 1.0/bins_per_decade)
        for a in range(0, len(solar_model_parameters)):
            outfile_name_timestep = root_output_filename + '_potential_bartels_' + str(int(solar_model_parameters[a][4])) + '.txt'
            outfile = open(outfile_name_timestep, 'w')
            for b in range(0, len(lg_energy_bins)):
                energyval = np.power(10.0, lg_energy_bins[b])
                print(energyval, modulation_potential_interpolated[a](energyval), file=outfile)
            outfile.close()
