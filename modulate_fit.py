##This is the main fitting algorithm in the program, which we loop over.
import math
import sys
import numpy as np
from scipy import interpolate

rad2deg = 180.0/math.pi

##Fitting at a single value of phi0/phi1 is really a two step process
##First, we need to go through and calculate all the modulation potentials that are affecting our signal. This is a rigidity dependent statement
####So we want a list of the solar modulation potential at every rigidity and every bartels rotation
####Then, we can use this information, along with the relative weighting of each bartels rotation -- in order to calculate a new spectrum from the model
#####Note: We already have the equivalent values of rigidity and ekin_pn in every bin, so we can move between these units effortlessly
def calculate_modulation_potentials_for_phi0_phi1(phi0, phi1, rigidity, ekin, beta, solar_model_parameters, interpolation_order):
    ##In this first step, we just calculate the modulation potential in every rigidity bin and in every bartels rotation, we return a 2D array of values
    ##There is some complexity. We want ot calculate the modulation potential array at ekin at Earth -- to use it in the function below.
    ##However, the modulation potential is defined at ekin in the ISM in Equation 1, and in rigidity in Equation 2 -- both of which are in different wrong units.
    ##So we need to start by getting ekin values, and using an interpolation object to get these values in ekin.
    ##Then we run another function that takes the charges and masses, and makes a new grid at ekin_earth - which we can finally use in our analysis

    ##We send the timesteps and the timestep weights into this function, so we only need to calculate them once
    ##Set up definitions for function
    R0=0.5 ##Defined in 1511.01507
    modulation_potential_interpolated = [] ##1D array of an interpolated object at every Bartels rotation

    for bartels_rotation in range(0, len(solar_model_parameters)):
        rigidity_term = (1.0 + np.power(rigidity/R0, 2.0)) / (beta * np.power(rigidity/R0, 3.0))
        alpha_term = np.power(solar_model_parameters[bartels_rotation][1]/rad2deg*2.0/math.pi, 4.0)
        phi0_term = phi0 * solar_model_parameters[bartels_rotation][0] / 4.0 ##Bval in nT
        phi1_term = phi1 * solar_model_parameters[bartels_rotation][2] * (solar_model_parameters[bartels_rotation][0]/4.0) * rigidity_term * alpha_term
        modulation_potential_array_for_interpolator = phi0_term + phi1_term

        ##Test values before interpolation
        modulation_potential_interpolated += [interpolate.interp1d(ekin, modulation_potential_array_for_interpolator, kind=interpolation_order)] ##This gets you into the right ekin total
    return modulation_potential_interpolated

def transfer_modulation_potential_ism_to_earth(modulation_potential_interpolated, ekin, particle_mass, abs_particle_charge, solar_model_parameters, interpolation_order):
    ##This takes an array of modulation potentials (at each Bartels rotation) that are defined in ekin.
    ##It shifts the x-axis of the interpolated object to go from ekin in the ISM, to ekin at earth
    ##It returns a 2D modulation_potential_array, of the modulation potential at Earth for each bartels rotation and ekin at Earth. Doing it this way makes the next step easy matrix multiplication
    modulation_potential_array = np.zeros((len(solar_model_parameters), len(ekin)))
    for bartels_rotation in range(0, len(solar_model_parameters)):
        energies_earth = []
        modulation_potential_earth = []
        for energy_ism in range(0, len(ekin)): ##Go through the ISM values, and find the new Earth values (might be negative)
            energies_earth += [ekin[energy_ism] - abs_particle_charge * modulation_potential_interpolated[bartels_rotation](ekin[energy_ism])]
            modulation_potential_earth += [modulation_potential_interpolated[bartels_rotation](ekin[energy_ism])]
        interpolated_result_at_earth = interpolate.interp1d(energies_earth, modulation_potential_earth, kind=interpolation_order, bounds_error=False, fill_value=modulation_potential_earth[-1]) ##The highest datapoint will fail, assume the modulation is the same as in the last bin
        #interpolated_backwards_test = interpolate.interp1d(energies_earth, ekin, kind=interpolation_order, bounds_error=False, fill_value=ekin[-1])
        #print ("Using Interpolation Order: ", interpolation_order)
        ##Now go through and build a final grid -- the last point will technically get the value at the top of the box, which is fine
        for energy_earth in range(0, len(ekin)):
            modulation_potential_array[bartels_rotation][energy_earth] = interpolated_result_at_earth(ekin[energy_earth])
    return modulation_potential_array

def fast__transfer_modulation_potential_ism_to_earth(modulation_potential_interpolated, ekin, particle_mass, abs_particle_charge, solar_model_parameters, interpolation_order):
    ## This function does exactly the same as fast__transfer_modulation_potential_ism_to_earth. The decrease in time is about a factor 10.

    ##This takes an array of modulation potentials (at each Bartels rotation) that are defined in ekin.
    ##It shifts the x-axis of the interpolated object to go from ekin in the ISM, to ekin at earth
    ##It returns a 2D modulation_potential_array, of the modulation potential at Earth for each bartels rotation and ekin at Earth. Doing it this way makes the next step easy matrix multiplication
    modulation_potential_array = np.zeros( (len(solar_model_parameters), len(ekin)) )
    for b in range(len(solar_model_parameters)):
        ekin = np.maximum( ekin, 1e-90)
        energies_earth = np.maximum( ekin - abs_particle_charge * modulation_potential_interpolated[b](ekin), 1e-90 )
        modulation_potential_earth = np.maximum( modulation_potential_interpolated[b](ekin), 1e-90 )
        modulation_potential_array[b,:] = np.exp(  np.interp( np.log(ekin), np.log(energies_earth), np.log(modulation_potential_earth), left=np.log(modulation_potential_earth[-1]), right=np.log(modulation_potential_earth[-1]) )   )
    return modulation_potential_array


def calculate_modulation_potentials_for_phi0_phi1__and__transfer_modulation_potential_ism_to_earth(phi0, phi1, rigidity, ekin, beta, particle_mass, abs_particle_charge, solar_model_parameters ):
    ##This function combines the functions calculate_modulation_potentials_for_phi0_phi1 and transfer_modulation_potential_ism_to_earth.
    ##The increase in speed is a factor of about 150.
    ##
    ##In this first step, we just calculate the modulation potential in every rigidity bin and in every bartels rotation, we return a 2D array of values
    ##Then the modulation potential is interpolated to ekin at earth.
    ##It returns a 2D modulation_potential_array, of the modulation potential at Earth for each bartels rotation and ekin at Earth. Doing it this way makes the next step easy matrix multiplication
    modulation_potential_array = np.zeros( (len(solar_model_parameters), len(ekin)) )
    R0=0.5 ##Defined in 1511.01507
    for bartels_rotation in range(len(solar_model_parameters)):
        rigidity_term = (1.0 + np.power(rigidity/R0, 2.0)) / (beta * np.power(rigidity/R0, 3.0))
        alpha_term = np.power(solar_model_parameters[bartels_rotation][1]/rad2deg*2.0/math.pi, 4.0)
        phi0_term = phi0 * solar_model_parameters[bartels_rotation][0] / 4.0 ##Bval in nT
        phi1_term = phi1 * solar_model_parameters[bartels_rotation][2] * (solar_model_parameters[bartels_rotation][0]/4.0) * rigidity_term * alpha_term
        modulation_potential_array_for_interpolator = phi0_term + phi1_term
        #
        energies_earth = np.maximum( ekin - abs_particle_charge * modulation_potential_array_for_interpolator, 1e-90 )
        modulation_potential_earth = np.maximum( modulation_potential_array_for_interpolator, 1e-90 )
        modulation_potential_array[bartels_rotation,:] = np.exp(  np.interp( np.log(ekin), np.log(energies_earth), np.log(modulation_potential_earth), left=np.log(modulation_potential_earth[-1]), right=np.log(modulation_potential_earth[-1]) )   )
    return modulation_potential_array


##This code assumes Ekin -- probably need to make it so that the code internally uses that for the ISM spectrum
def calculate_ekin_at_earth_from_ekin_ism_using_modulation_parameters(ekin, ism_flux_interpolation_object, modulation_potential_array, solar_model_totalweight, particle_mass, abs_particle_charge):
    ##Can save time by doing this in in numpy arrays, rather than in a for loop for energies, but will need a for loop
    term1 = (np.power(ekin + particle_mass, 2.0) - np.power(particle_mass, 2.0)) / (np.power(ekin + particle_mass + abs_particle_charge * modulation_potential_array, 2.0) - np.power(particle_mass, 2.0))
    term2 = ism_flux_interpolation_object(ekin + abs_particle_charge * modulation_potential_array)
    result = term1 * term2
    return result

def get_average_spectrum_over_full_observation(influx, solar_model_parameters):
    total_exposure = np.sum(solar_model_parameters[:,3])
    outflux = 0
    for a in range(0, len(solar_model_parameters[:,3])):
        outflux += solar_model_parameters[:,3][a] * influx[a] / total_exposure
    return outflux
